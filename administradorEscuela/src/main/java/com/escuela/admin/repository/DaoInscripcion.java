package com.escuela.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.escuela.admin.model.Inscripcion;

public interface DaoInscripcion extends JpaRepository<Inscripcion, Long> {

}
