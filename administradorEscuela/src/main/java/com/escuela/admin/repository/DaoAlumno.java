package com.escuela.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.escuela.admin.model.Alumno;

public interface DaoAlumno extends JpaRepository<Alumno, Long> {

}
