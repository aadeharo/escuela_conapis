package com.escuela.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.escuela.admin.model.Curso;

public interface DaoCurso extends JpaRepository<Curso, Long> {

}
