package com.escuela.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.escuela.admin.model.Colegio;

public interface DaoColegio extends JpaRepository<Colegio, Long> {

}
