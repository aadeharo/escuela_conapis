package com.escuela.admin.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Curso")
public class Curso {

	@Id				
    @GeneratedValue	
	private Long id;
	
	private String name;
	private int cupo;
	private float notaAprobacion;
	
	@OneToMany(cascade=CascadeType.ALL,mappedBy="curso")
	public List<Inscripcion> inscripciones;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="colegio_id")
	private Colegio colegio;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Inscripcion> getInscripciones() {
		return inscripciones;
	}

	public void setInscripciones(List<Inscripcion> inscripciones) {
		this.inscripciones = inscripciones;
	}

	public Colegio getColegio() {
		return colegio;
	}

	public void setColegio(Colegio colegio) {
		this.colegio = colegio;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getCupo() {
		return cupo;
	}
	
	public void setCupo(int cupo) {
		this.cupo = cupo;
	}
	
	public float getNotaAprobacion() {
		return notaAprobacion;
	}
	
	public void setNotaAprobacion(float notaAprobacion) {
		this.notaAprobacion = notaAprobacion;
	}
		
	
}
