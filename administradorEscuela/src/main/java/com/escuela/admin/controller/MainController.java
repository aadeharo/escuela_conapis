package com.escuela.admin.controller;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.escuela.admin.model.Alumno;
import com.escuela.admin.model.Curso;
import com.escuela.admin.model.Inscripcion;
import com.escuela.admin.repository.DaoAlumno;
import com.escuela.admin.repository.DaoColegio;
import com.escuela.admin.repository.DaoCurso;
import com.escuela.admin.repository.DaoInscripcion;

@RestController
@RequestMapping
public class MainController {
	
	@Autowired
	private DaoColegio daoColegio;
	
	@Autowired
	private DaoCurso daoCurso;
	
	@Autowired
	private DaoInscripcion daoInscripcion;
	
	@Autowired
	private DaoAlumno daoAlumno;
	
	/* 			APIS
	 * 
	 *  ENTREGAR LISTA DE CURSOS
	 *  ENTREGAR LISTA DE INSCRIPCIONES
	 *  ENTREGAR LISTA DE ALUMNOS
	 *  
	 *  SETEAR ALUMNO, SETEAR INSCRIPCION A CURSO
	 *  
	 *  CREAR CURSO
	 * 
	 * */
	
	//		CREAR CURSO
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping(path = { "/nuevocurso" })
	public Curso createCurso(@RequestBody Curso curso)
	{
		
		return daoCurso.save(curso);
	
	}//      /nuevocurso
	
	@GetMapping({ "/listarcursos" })
	public ResponseEntity<Object> listarCursos()
	{
	
		
		
		return null;
	}
	
	
    //	    BORRAR CURSO
	@CrossOrigin(origins = "http://localhost:4200")
	@DeleteMapping({ "/deletecurso/{curId}" })
	public ResponseEntity<Object> deleteCurso(@PathVariable("curId") Long curId)
	{
		JSONObject obj = new JSONObject();
		
		daoCurso.deleteById(curId);
	
		obj.put("error", 0);
		obj.put("message", "Se eliminó correctamente el curso");
			
		return ResponseEntity.ok().body(obj.toString());	
	
	}//     /deletecurso
	
	
	//	CREAR INSCRIPCION
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping({"{curId}/nuevainscripcion/{aluId}" })
	public ResponseEntity<Object> nuevaInscripcion(@PathVariable("curId") Long curId, @PathVariable("aluId") Long aluId)
	{
		JSONObject obj = new JSONObject();
		
		Curso cur = daoCurso.findById(curId).orElse(null);
		
		Inscripcion ins = new Inscripcion();
		ins.setCurso(cur);
		
		Alumno alum = daoAlumno.findById(aluId).orElse(null);
		alum.setInscripcion(ins);
		daoAlumno.save(alum);
		
		
		daoInscripcion.save(ins);
		
		obj.put("error", 0);
		obj.put("message", "Se ha realizado la inscripcion correctamente");
		
		return ResponseEntity.ok().body(obj.toString());
		
	}//     {curId}/nuevainscripcion/{aluId}
	
	
	//		CREAR ALUMNO
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping({ "/nuevoalumno" })
	public ResponseEntity<Object> createChicken(@RequestBody Alumno alumno) {
		
		Alumno alu = new Alumno();
		
		alu.setName(alumno.getName());
		alu.setLname(alumno.getLname());
		alu.setDni(alumno.getDni());
		alu.setAge(alumno.getAge());

		daoAlumno.save(alu);
		
		JSONObject obj = new JSONObject();
		obj.put("error", 0);
		obj.put("message", "Alumno creado: " + alu.getLname() + ", " + alu.getName() + ". ID: " + alu.getId());

		return ResponseEntity.ok().body(obj.toString());

	}//     /nuevoalumno
	
	
	//	BORRAR ALUMNO
	@CrossOrigin(origins = "http://localhost:4200")
	@DeleteMapping({ "/deletealumno/{aluId}" })
	public ResponseEntity<Object> deleteAlumno(@PathVariable("aluId") Long aluId)
	{
	
		JSONObject obj = new JSONObject();
		
		daoAlumno.deleteById(aluId);
	
		obj.put("error", 0);
		obj.put("message", "Se eliminó correctamente al alumno");
			
		return ResponseEntity.ok().body(obj.toString());	
		
	}//     /deletealumno
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
